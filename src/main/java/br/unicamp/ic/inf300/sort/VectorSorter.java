package main.java.br.unicamp.ic.inf300.sort;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

public class VectorSorter {
	
	private static final int RANDOM_VALUE_MIN = 1;
	private static final int RANDOM_VALUE_MAX = 100;
	private static final int DEFAULT_VECTOR_SIZE = 10;
	private static Logger logger= LogManager.getLogger(VectorSorter.class);

	public static void main(String[] args) {

		int[] numbers = parseParameters(args);
	
		logger.info ("Input: ");
		printVector(numbers);
		sort(numbers);
		logger.info ("Sorted: ");
		printVector(numbers);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(DEFAULT_VECTOR_SIZE);
		}
		return numbers;
	}
	
	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 0; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()*RANDOM_VALUE_MAX + RANDOM_VALUE_MIN);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers) {
		
		String msg;
		msg = "[ ";
		msg += Integer.toString(numbers[0]);
		
		int i = 0;
		do {
			i++;
			msg += ", ";
			msg += Integer.toString(numbers[i]);
		}while(i < numbers.length - 1);

		msg += " ]";
		logger.info(msg);
	}
}
