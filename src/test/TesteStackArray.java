package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import main.java.br.unicamp.ic.inf300.sort.StackArray;

public class TesteStackArray {

	@Test
	public void testeTamanhoFilaInicial() {
		StackArray stackArray = new StackArray();
		int tamanho = stackArray.size();
		assertEquals(tamanho, 0);
	}

	@Test
	public void testeTamanhoFilaAposInsercoes() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		int tamanho = stackArray.size();
		assertEquals(tamanho, 4);
	}

	@Test
	public void testeTamanhoFilaAposInsercoesERetiradas() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		stackArray.pop();

		int tamanho = stackArray.size();
		assertEquals(tamanho, 3);
	}

	@Test
	public void testeTamanhoFilaAposInsercoesERetiradaTotal() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		stackArray.pop();
		stackArray.pop();
		stackArray.pop();
		stackArray.pop();

		int tamanho = stackArray.size();
		assertEquals(tamanho, 0);
	}

	@Test
	public void testeValorTopoFilaVazia() {
		StackArray stackArray = new StackArray();

		assertEquals(stackArray.peek(), -1);
	}

	@Test
	public void testeValorTopoFilaAposInsercoes() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		assertEquals(stackArray.peek(), 13000);
	}

	@Test
	public void testeValorTopoFilaAposInsercoesERemocao() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		stackArray.pop();

		assertEquals(stackArray.peek(), 1300);
	}

	@Test
	public void testeValorTopoFilaAposInsercoesERemocoes() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		stackArray.pop();
		stackArray.pop();

		assertEquals(stackArray.peek(), 130);
	}

	@Test
	public void testeValorRetiradoFilaAposInsercoes() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		assertEquals(stackArray.pop(), 13000);
	}

	@Test
	public void testeValorRetiradoFilaAposInsercoesERemocao() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);

		stackArray.pop();

		assertEquals(stackArray.pop(), 1300);
	}

	@Test
	public void testeLimiteMaximo() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);
		stackArray.push(130000);
		stackArray.push(1300000);
		stackArray.push(13000000);
		stackArray.push(130000000);
		stackArray.push(1300000000);
		stackArray.push(131);

		assertEquals(stackArray.size(), 10);
	}

	@Test
	public void testeExcederLimiteMaximo() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);
		stackArray.push(130000);
		stackArray.push(1300000);
		stackArray.push(13000000);
		stackArray.push(130000000);
		stackArray.push(1300000000);
		stackArray.push(131);
		stackArray.push(1312);

		assertEquals(stackArray.size(), 11);
	}

	@Test
	public void testeExcederLimiteRemocoesAposInsercoes() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);

		stackArray.pop();
		stackArray.pop();
		stackArray.pop();

		assertEquals(stackArray.size(), 0);
	}

	@Test
	public void testeExcederLimiteRemocoes() {
		StackArray stackArray = new StackArray();

		stackArray.pop();

		assertEquals(stackArray.size(), 0);
	}

	@Test
	public void testeVerificarVazio() {
		StackArray stackArray = new StackArray();

		assertTrue(stackArray.isEmpty());
	}

	@Test
	public void testeVerificarVazioComElementoInserido() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);

		assertFalse(stackArray.isEmpty());
	}

	@Test
	public void testeVerificarCheioSobrandoPosicao() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);
		stackArray.push(130000);
		stackArray.push(1300000);
		stackArray.push(13000000);
		stackArray.push(130000000);
		stackArray.push(1300000000);

		assertFalse(stackArray.isFull());
	}

	@Test
	public void testeVerificarCheio() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);
		stackArray.push(130000);
		stackArray.push(1300000);
		stackArray.push(13000000);
		stackArray.push(130000000);
		stackArray.push(1300000000);
		stackArray.push(131);

		assertTrue(stackArray.isFull());
	}

	@Test
	public void testeVerificarFazerVazio() {
		StackArray stackArray = new StackArray();

		stackArray.push(13);
		stackArray.push(130);
		stackArray.push(1300);
		stackArray.push(13000);
		stackArray.push(130000);
		stackArray.push(1300000);
		stackArray.push(13000000);
		stackArray.push(130000000);
		stackArray.push(1300000000);
		stackArray.push(131);

		stackArray.makeEmpty();

		assertTrue(stackArray.isEmpty());
	}

	@Test
	public void testeVerificarOrdemRemovidos() {
		int[] valores = { 13, 130, 1300, 13000, 130000, 130000, 1300000, 130000000, 1300000000, 131 };

		StackArray stackArray = new StackArray();

		for (int valor : valores) {
			stackArray.push(valor);
		}

		for (int i = valores.length - 1; i >= 0; i--) {
			assertEquals(stackArray.pop(), valores[i]);
		}
	}
}
